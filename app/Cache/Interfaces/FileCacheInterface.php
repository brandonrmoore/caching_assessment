<?php

declare(strict_types=1);

namespace App\Cache\Interfaces;

use Carbon\Carbon;

/**
 * Interface FileCacheInterface
 * @package App\Cache\Interfaces
 */
interface FileCacheInterface
{
    /**
     * @param mixed $value
     * @return FileCacheInterface
     */
    public function setValue($value): FileCacheInterface;

    /**
     * @param Carbon|null $expirationDate
     * @return FileCacheInterface
     */
    public function setExpirationDate(?Carbon $expirationDate): FileCacheInterface;

    /**
     * @return string|null
     */
    public function getValue(): ?string;

    /**
     * @return Carbon|null
     */
    public function getExpirationDate(): ?Carbon;

    /**
     * @return bool
     */
    public function isExpired(): bool;
}
