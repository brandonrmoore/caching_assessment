<?php

declare(strict_types=1);

namespace App\Cache\Interfaces;

/**
 * Interface CacheServiceInterface
 * @package App\Cache\Interfaces
 */
interface CacheServiceInterface
{
    /**
     * @param string $key
     * @return string|null
     */
    public function get(string $key): ?string;

    /**
     * @param string $key
     * @param $value
     * @param int|null $expirationSeconds
     * @return bool
     */
    public function set(string $key, $value, ?int $expirationSeconds = null): bool;

    /**
     * @param string ...$keys
     * @return bool
     */
    public function delete(string ...$keys): bool;
}
