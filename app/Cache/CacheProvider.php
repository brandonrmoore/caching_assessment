<?php

declare(strict_types=1);

namespace App\Cache;

use App\Cache\Interfaces\CacheServiceInterface;
use App\Cache\Services\FileCacheService;
use App\Cache\Services\RedisCacheService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Illuminate\Config\Repository as ConfigRepository;

/**
 * Class CacheProvider
 * @package App\Cache
 */
class CacheProvider extends ServiceProvider
{
    /**
     * @inheritDoc
     */
    public function register()
    {
        $this->registerServices();
    }

    /**
     * Registers services
     */
    private function registerServices()
    {
        $this->app->bind(CacheServiceInterface::class, function (Application $app) {
            $configRepository = $app->make(ConfigRepository::class);
            switch ($configRepository->get('cache.default')) {
                case "redis":
                    return new RedisCacheService();
                case "file":
                default:
                    return new FileCacheService();
            }
        });
    }
}
