<?php

declare(strict_types=1);

namespace App\Cache\Services;

use App\Cache\Interfaces\CacheServiceInterface;
use App\Cache\Objects\FileCache;
use Carbon\Carbon;

/**
 * Class FileCacheService
 * @package App\Cache\Services
 */
class FileCacheService implements CacheServiceInterface
{

    /**
     * @param string $key
     * @return string|null
     */
    public function get(string $key): ?string
    {
        if (!file_exists($this->getFullFilePath($key))) {
            return null;
        }
        return $this->getCacheValue($key);
    }

    /**
     * @param string $key
     * @param $value
     * @param int $expirationSeconds
     * @return bool
     */
    public function set(string $key, $value, ?int $expirationSeconds = null): bool
    {
        $filePath = $this->getFullFilePath($key);
        $directory = dirname($filePath);
        try {
            if (!file_exists($directory)) {
                mkdir($directory, 0755, true);
            }
            $fileCache = new FileCache([
                'value' => $value
            ]);
            if ($expirationSeconds > 0) {
                $fileCache->setExpirationDate(
                    Carbon::now()->addSeconds($expirationSeconds)
                );
            }
            file_put_contents($filePath, serialize($fileCache));
        } catch (\Throwable $e) {
            return false;
        }
        return true;
    }

    /**
     * @param string ...$keys
     * @return bool
     */
    public function delete(string ...$keys): bool
    {
        foreach ($keys as $key) {
            try {
                unlink($this->getFullFilePath($key));
            } catch (\Throwable $e) {
                continue;
            }
        }
        return true;
    }

    /**
     * @return string
     */
    private function getRootDirectory(): string
    {
        return __DIR__ . "/../../../storage/cache";
    }

    /**
     * @param string $key
     * @return string
     */
    private function getFullFilePath(string $key): string
    {
        $hash = md5($key);
        $firstSubDirectory = substr($hash, 0, 1);
        $secondSubDirectory = substr($hash, 0, 2);
        return $this->getRootDirectory() .
            "/$firstSubDirectory" .
            "/$secondSubDirectory" .
            "/$hash.json";
    }

    /**
     * @param string $key
     * @return string|null
     */
    private function getCacheValue(string $key): ?string
    {
        $filePath = $this->getFullFilePath($key);
        try {
            $fileCache = unserialize(file_get_contents($filePath));
            if ($fileCache->isExpired()) {
                unlink($filePath);
                return null;
            }
            return $fileCache->getValue();
        } catch (\Throwable $e) {
            return null;
        }
    }
}
