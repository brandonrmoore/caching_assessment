<?php

declare(strict_types=1);

namespace App\Cache\Services;

use App\Cache\Interfaces\CacheServiceInterface;
use Illuminate\Support\Facades\Redis;
use Predis\Client;

/**
 * Class RedisCacheService
 * @package App\Cache\Services
 */
class RedisCacheService implements CacheServiceInterface
{

    /**
     * @param string $key
     * @return string|null
     */
    public function get(string $key): ?string
    {
        return $this->client()->get($key);
    }

    /**
     * @param string $key
     * @param $value
     * @param int $expirationSeconds
     * @return bool
     */
    public function set(string $key, $value, ?int $expirationSeconds = null): bool
    {
        if ($expirationSeconds === null || $expirationSeconds === 0) {
            return (bool)$this->client()->set($key, $value);
        }
        return (bool)$this->client()->setex($key, $expirationSeconds, $value);
    }

    /**
     * @param string ...$keys
     * @return bool
     */
    public function delete(string ...$keys): bool
    {
        return (bool)$this->client()->del($keys);
    }

    /**
     * @return Client
     */
    private function client(): Client
    {
        return Redis::connection()->client();
    }
}
