<?php

declare(strict_types=1);

namespace App\Cache\Objects;

/**
 * Class AbstractCacheObject
 * @package App\Cache\Objects
 */
abstract class AbstractCacheObject
{
    /**
     * AbstractCacheObject constructor.
     * @param array $payload
     */
    public function __construct(array $payload = [])
    {
        //attempt to call setters if they exist after transforming to an expected setter
        foreach ($payload as $key => $value) {
            $method = 'set';
            $method .= str_replace(" ", "", ucwords(strtolower(str_replace(["-", "_"], " ", $key))));
            if (!method_exists($this, $method)) {
                continue;
            }
            call_user_func_array([$this, $method], [$value]);
        }
    }
}
