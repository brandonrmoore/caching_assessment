<?php

declare(strict_types=1);

namespace App\Cache\Objects;

use App\Cache\Interfaces\FileCacheInterface;
use Carbon\Carbon;

class FileCache extends AbstractCacheObject implements FileCacheInterface
{
    /**
     * @var string|null
     */
    private $value = null;

    /**
     * @var Carbon|null
     */
    private $expirationDate = null;

    /**
     * @param mixed $value
     * @return FileCacheInterface
     */
    public function setValue($value): FileCacheInterface
    {
        $this->value = (string)$value;
        return $this;
    }

    /**
     * @param Carbon|null $expirationDate
     * @return FileCacheInterface
     */
    public function setExpirationDate(?Carbon $expirationDate): FileCacheInterface
    {
        $this->expirationDate = $expirationDate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        if ($this->value === null) {
            return null;
        }
        return (string)$this->value;
    }

    /**
     * @return Carbon|null
     */
    public function getExpirationDate(): ?Carbon
    {
        return $this->expirationDate;
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        if (!$this->expirationDate) {
            return false;
        }
        if (Carbon::now()->lt($this->expirationDate)) {
            return false;
        }
        return true;
    }
}
