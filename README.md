# About This Repo

Provides an example of using a standard contract with multiple implementations for different types of caching.

While the code uses Laravel, the bulk of the solution to the assessment is under `app/Cache`

## Docker

For local development docker config files have been included. To get started and into the PHP terminal:

```
docker-compose build
docker-compose up -d
docker-compose exec php bash
cd /var/www/html
composer install
```

## The Caching Code

### Default configs

The `.env` file specifies the default `CACHE_DRIVER` as `redis`. This can be changed to `file`. When doing so the cache will need to be cleared

```
php artisan config:clear
```

### Instantiating the cache services

The IoC has been configured to the `CacheServiceInterface` contract. 

```
php artisan tinker
use App\Cache\Interfaces\CacheServiceInterface;
$service = App::make(CacheServiceInterface::class);
=> App\Cache\Services\RedisCacheService {#2999}
```

Because the IoC is being used, code can swap out the mapping to any other available driver on the fly

```
php artisan tinker
use App\Cache\Interfaces\CacheServiceInterface;
use App\Cache\Services\FileCacheService;
App::bind(CacheServiceInterface::class, FileCacheService::class);
$service = App::make(CacheServiceInterface::class);
=> App\Cache\Services\FileCacheService {#3001}
```

### Using the cache service

#### Getting caches

```
php artisan tinker
use App\Cache\Interfaces\CacheServiceInterface;
$service = App::make(CacheServiceInterface::class);
=> App\Cache\Services\RedisCacheService {#2999}
$service->get('key');
=> null
```

#### Setting a cache

```
php artisan tinker
use App\Cache\Interfaces\CacheServiceInterface;
$service = App::make(CacheServiceInterface::class);
=> App\Cache\Services\RedisCacheService {#2999}
$service->set('key', 'pretty sweet value');
=> true
>>> $service->get('key');
=> "pretty sweet value"
```

#### Setting a cache with a TTL

```
php artisan tinker
use App\Cache\Interfaces\CacheServiceInterface;
$service = App::make(CacheServiceInterface::class);
$service->set('key', 'pretty sweet value', 10);
=> true
>>> $service->get('key');
=> "pretty sweet value"
sleep(10);
$service->get('key');
=> null
```

#### Manually deleting keys

```
php artisan tinker
use App\Cache\Interfaces\CacheServiceInterface;
$service = App::make(CacheServiceInterface::class);
$service->set('key1', 1);
=> true
$service->set('key2', 2);
=> true
$service->get('key1');
=> "1"
$service->get('key2');
=> "2"
$service->delete('key1', 'key2');
=> true
$service->get('key1');
=> null
$service->get('key2');
=> null
```
